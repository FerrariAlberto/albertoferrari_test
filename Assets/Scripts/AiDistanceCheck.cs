﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AiDistanceCheck : MonoBehaviour
{
    public float[] ranges; 
    public float waitTime;

    private TextMeshPro _text;
    private float _sqrDist, _SqrtDist;
    public int _currentRangeCheck;


    private void Start()
    {
        _text = transform.GetChild(0).GetComponent<TextMeshPro>();

        //checks from singleton if player is already spawned and subscribes this Ai to the player spawn event
        GameManager.instance.onPlayerSpawn += StartCheck;
        if (GameManager.instance.player)
        {
            StartCheck();
        }
    }

    //order ranges array and initializes range check from the biggest one
    public void StartCheck()
    {
        if (ranges.Length > 0)
        {
            Array.Sort(ranges);
            _currentRangeCheck = ranges.Length - 1;
            CheckDistance();
        }
        else
            Debug.Log(gameObject.name + " doesn't have any ranges");
    }

    //Checks for the minimum time a player with known max speed would need to reach the ranges[_currentRangeCheck] range and
    //recalls itself waiting that time span
    public void CheckDistance()
    {
        CancelInvoke();
        _sqrDist = SqrDistance(transform.position, GameManager.instance.player.transform.position);

        //checks if my distance from the player is bigger then the range i'm checking, if not changes range checked to a smaller one
        //(compares square distances to save performance and only calls the square root function when really needed)
        if (_sqrDist > ranges[_currentRangeCheck] * ranges[_currentRangeCheck])
        {
            //checks if the player is not walking towards and changes the range checked to a bigger one
            if(_currentRangeCheck + 1 <= ranges.Length -1 && _sqrDist > ranges[_currentRangeCheck + 1] * ranges[_currentRangeCheck + 1])
                _currentRangeCheck += 1;

            //the time span (waitTime) is equal to my distance from the player minus the range i'm checking divided by the player max speed
            _SqrtDist = (float)Mathf.Sqrt(_sqrDist);
            waitTime = (_SqrtDist - ranges[_currentRangeCheck]) / GameManager.instance.player.Speed < .15f ? waitTime = .15f : (_SqrtDist - ranges[_currentRangeCheck]) / GameManager.instance.player.Speed;
            _text.text = "inside range<br>" + ranges[_currentRangeCheck + 1 <= ranges.Length-1? _currentRangeCheck +1 : _currentRangeCheck] + "<br>every: " + waitTime.ToString("F2");
            Invoke("CheckDistance", waitTime);
        }
        else
        {
            if(_currentRangeCheck - 1 >= 0) _currentRangeCheck -= 1;
            Invoke("CheckDistance", waitTime);
        }
    }

    //returns the square distance between two Vector3
    public static float SqrDistance(Vector3 pos1, Vector3 pos2)
    {
        Vector3 resultingVector = new Vector3(pos1.x - pos2.x, pos1.y - pos2.y, pos1.z - pos2.z);
        return resultingVector.x * resultingVector.x + resultingVector.y * resultingVector.y + resultingVector.z * resultingVector.z;
    }

    //returns the distance between two Vector3
    public static float SqrtDistance(Vector3 pos1, Vector3 pos2)
    {
        Vector3 resultingVector = new Vector3(pos1.x - pos2.x, pos1.y - pos2.y, pos1.z - pos2.z);
        return Mathf.Sqrt(resultingVector.x * resultingVector.x + resultingVector.y * resultingVector.y + resultingVector.z * resultingVector.z);
    }

    //Debug: draws a gizmos wire green sphere for the current range checked and a rad one for the old larger range 
    void OnDrawGizmos()
    {
        if (GameManager.instance.player && ranges.Length > 0)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, ranges[_currentRangeCheck]);
            if (_currentRangeCheck + 1 <= ranges.Length-1)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawWireSphere(transform.position, ranges[_currentRangeCheck + 1]);
            }
        }
    }

    private void OnDestroy()
    {
        GameManager.instance.onPlayerSpawn -= StartCheck;
    }
}
