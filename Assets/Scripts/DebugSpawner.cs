﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugSpawner : MonoBehaviour
{
    public int maxEnemies;
    public Vector2 grid;
    public float enemyRadius;

    public GameObject enemyPrefab, playerPrefab;

    int enNum;
    public Slider slider;
    public Text text;

    //updates grid values in editor window
    private void OnValidate()
    {
        if (maxEnemies < 0) maxEnemies = 0;
        grid.x = Mathf.CeilToInt(Mathf.Sqrt(maxEnemies));
        grid.y = grid.x;
    }

    //Functions called from UI
    //checks if player is already spawned, if not spawns a new one and triggers the player spawn event
    public void SpawnPlayer()
    {
        if (GameManager.instance.player == null)
        {
            GameManager.instance.player = Instantiate(playerPrefab, new Vector3(0, 1, -10), Quaternion.identity).GetComponent<Player>();
            GameManager.instance.PlayerSpawn();
        }
    }

    //Gets UI slider value and updates maxEnemiess number
    public void UpdateSlider()
    {
        maxEnemies = Mathf.RoundToInt(slider.value);
        text.text = maxEnemies.ToString();
    }

    //Spawns maxEnemies on a grid
    public void SpawnEnemies()
    {
        //check and destroy already spawned enemies
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        //updates grid size to fit in maxEnemies 
        if (maxEnemies < 0) maxEnemies = 0;
        grid.x = Mathf.CeilToInt(Mathf.Sqrt(maxEnemies));
        grid.y = grid.x;

        enNum = 0;
        float cellSize = enemyRadius / Mathf.Sqrt(2);

        //spawns enemies on the grid
        for (int i = 0; i < grid.x; i++)
        {
            for (int j = 0; j < grid.y; j++)
            {
                if (enNum < maxEnemies)
                {
                    GameObject enemy = Instantiate(enemyPrefab, new Vector3(cellSize * i, 1f, cellSize * j), Quaternion.identity, transform);
                    enemy.name = "Enemy_" + enNum;
                    enNum++;
                }
            }
        }
    }

    
}
