﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public Player player;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }


    //Sets up player spawn event to which enemies will subscribe to
    public event Action onPlayerSpawn;
    public void PlayerSpawn()
    {
        if(onPlayerSpawn != null)
        {
            onPlayerSpawn();
        }
    }

}
