﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // a simple rigidbody movement

    public float Speed = 5f, rotationSpeed = 1.5f;

    private Rigidbody _body;
    private Vector3 _inputDirection, _moveDirection;

    void Start()
    {
        _body = GetComponent<Rigidbody>();
    }

    void Update()
    {
        _inputDirection = (new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"))).normalized;
        _moveDirection = transform.TransformDirection(_inputDirection);
    }


    void FixedUpdate()
    {
        _body.MovePosition(transform.position + _moveDirection * Speed * Time.fixedDeltaTime);
        _body.MoveRotation(Quaternion.Euler(_body.rotation.eulerAngles + new Vector3(0f, rotationSpeed * Input.GetAxis("Mouse X"), 0f)));
    }
}
